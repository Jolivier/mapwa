# Procédure de création d'un nouveau projet de carte

Pré requis:
- Avoir un compte github
- Avoir installé [git](https://git-scm.com/downloads) et [Tiled](https://www.mapeditor.org/)
- Installer npm (Optionnel, pour faire tourner WorkAdventure en local)
- **TOUJOURS** repartir du repo initial de chez workadventure
- **UN** repo par projet

## Création du repo sur Github

Voir : https://workadventu.re/map-building

- Cliquer pour arriver au repo [Starter Kit](https://github.com/thecodingmachine/workadventure-map-starter-kit)
- Puis use this template 
- Choisir un nom pour ce nouveau projet de carte
- **cocher** : Include all branches
- Cliquer create repository from template
- Le nouveau repo du projet est généré


![Screenshot github use template](img/WATUTO-github-use-template.png "Screenshot Github")

## Faire une copie locale du repo

- Créer un dossier en local pour le projet
- Ouvrir une ligne de commande dans ce dossier

Copie du repo en local :

`git clone https://github.com/Jacques-Olivier-Farcy/nom-du-projet.git`

Configuration de git, spécifier l'e-mail :

`git config --global user.email "jacques-olivier@ouvaton.org"`

La carte par défaut est **map.json**

Il est possible de prendre un dossier de map existant ailleurs et de copier l'intégralité dans ce nouveau répertoire.

Le repo est à cette adresse :https://github.com/Jacques-Olivier-Farcy/nom-du-projet

Puis ajouter mes fichiers au repo local :
(à faire après chaque modification ou ajout de fichiers)

`git add .`

Pour voir l'état du repo local

`git status`

On commit :

`git commit -m "Ajout initial des fichiers du projet"`
(-m pour message)


Pousser la branche maître en ligne :

`git push origin master`

Sur l'interface web, il y a dans le menu à droite github-pages
Cliquer puis view deployment
La page web qui s'affiche permet de tester la map

Autre moyen d'accéder à la page des liens vers les maps :
Dans github web menu settings puis en bas à gauche pages
Lien du site publié

## Pour lancer la map en local :
Installer npd (node.js download)

npm install
Attendre qq minutes

npm start : permet de travailler directement sur la map

## Avec le client :
Montrer le lien github et  / ou partager l'écran en local.
