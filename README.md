# Map structure for WorkAdventure (**WA**)

![Screenshot cyber map](img/capture-cabine.png "Example of map")

 * Documentation of staging version : https://staging.workadventu.re/map-building
 
## Files and folder structure

- The main map is placed in a folder of the name of the project.
- Each map MUST have a different name.
- Each other maps are placed in a subfolder with the name of the map.
- Each map must be constructed to be used independently.
- If a script is used, it is placed in the same folder or subfolder of each map, with the same name of the map's folder, with the extension .js.
- All the ressources are in a subfolder called assets.
- If there is audio, a subfolder in assets is created.
- It is better to use several maps rather than one large map (for reasons of performance but also of fluidity of exchanges between people )
- A map of 100x60 tiles is a really big map.
- No accent in map or tileset names.
- As performance decreases with the number of people connected, avoid exceeding cards with more than XXX people is not advised.
- All graphic assets must have a size that is strictly a multiple of 32.
- Tiles that are no longer used or not used in the maps should be deleted.
- On a live (Youtube or others stream), it is better that the Jitsi Room is in silent
- In case of events, when a lot of people popup in the start zone, it is better to have a silent zone.


Example with project named **journey** with several **islands** maps:

```
─── journey
    ├── journey.js
    ├── journey.json
    ├── assets
    │   └── audio
    ├── Corse
    │   ├── Corse.json
    │   ├── Corse.js
    │   └── assets1
    │       └── audio1
    ├── EasterIsland
    │   ├── EasterIsland.json
    │   ├── EasterIsland.js
    │   └── assets2
    │       └── audio2
    ├── Sicily
    │   ├── Sicily.json
    │   ├── Sicily.js
    │   └── assets3
    │       └── audio3
    ├── …
```

## Layers

In the editor of the maps, [Tiled MapEditor](https://www.mapeditor.org/), we use layers to ensure the overlay of elements.
Items that are specific to WorkAdventure begins with upper case (Except **start** layer and **floorLayer**).

An example of a list of Layers for a map:

![Layers in Tiled](img/layers-tiled.png "Example of layers in Tiled")


### Layers specific for WorkAdventure :

- All the tiles used to activate WorkAdventure's functions are invisible on the map.
- All these layers are therefore placed at the bottom of the list of layers.
- A special tileset is used to place all of the specific features:

![Specific WA tileset](img/watileset.png "Specific tiles for WA")



| Layer name   | Description | properties | type   | fonction | example | Details |
|--------------|-------------|------------|--------|----------|---------|---------|
| **floorLayer**   | the layer where the character belongs | | the only object layer  | Also used to place rectangle objects| | | 
| | | | | | | |
| **ZONE0**-name   |                                           |            |        |                                        |         |                                                      |
| **AUDIO0**-name   | Zone to play a sound or music | playAudio | string | Play an audio file | | An url or path where the audio file is  
| **ENTRY0**-name |                                           | startLayer | bool   |                                        |         |                                                      |
| **EXIT1**-name1  |                                           | exitUrl    | string | Move to another map | |
| **EXIT0**-name  |                                           | exitUrl    | string | Move to another map                    | | |
| **JITSI1**-name1 |                                           |            | | | | |
| **JITSI0**-name0 | Zonne for a meeting room Jitsi    | jitsiRoom     | string   | 0-meeting-room-top-left | |  |
| **SILENT**       | A zone where people could not contact each other with audio/video |  silent | bool | | | |
| **COLLIDES**     | To handle collisions (Walls, furnitures…) | collides   | bool   | The character can not go to this place |         | This propertie needs to be placed on a specific tile |
| **start**        | The place where your character start      | | | | | |

## General map properties

- All these properties are in **string** type

| Property name   | Description |
|-----------------|-------------|
| **mapCopyright**    | CC-BY-SA 3.0:\n - http://creativecommons.org/licenses/by-sa/3.0/\n - See the file: cc-by-sa-3.0.txt\nGNU GPL 3.0:\n - http://www.gnu.org/licenses/gpl-3.0.html\n - See the file: gpl-3.0.txt\n\nAssets from: workadventure |
| **mapDescription**  | Custom map for client1 |
| **mapImage**        | path to a picture map.png (export image of the map from tiled and add on the folder racine of .json) |
| **mapLink**         | Could be a link to the Git repository of the map for example |
| **mapName**         | name of the map|


## Pop up zone in floorLayer
In the **floorLayer**, to define the area where a pop up will be displayed, we use the rectangle creation tool.

| Rectangle name   | Description | properties | type   | fonction | example | Details |
|--------------|-------------|------------|--------|----------|---------|---------|
| popUp0name0  | Name of the zone of the pop up 0  ||||||

These zones are connected to the layers called ZONE0…, see table above.

## Tips and tricks

- Playing with opacity of a layer in Tiled could help to make transparency of tiled.

![Screenshot cyber map](img/transparency.gif "Example of transparency")

- When using jitsiConfig property, if you want that Microphone and Video to be muted when entering the jitsi use this syntax in the string field in the property:

`{ "startWithAudioMuted": true, "startWithVideoMuted": true }`

### Other layers

- All the other layers used to create the visual of the map are in lower case.


| Layer name   | Description | fonction | Details |
|--------------|-------------|----------|---------|
| walls-up-1          | |||
| walls-up-0          | |||
| ground-up-0         | |||
|**floorLayer**       ||||
| walls-2-flowers     | |||
| objects-1-computers  ||||
| objects-0           ||||
| objects-down-0      ||||
| walls-2-windows    | |||
| walls-1-glass      ||||
| walls-0            ||||
| walls-down-0        ||||
| ground-2-flowers   | |||
| ground-1-grass     ||||
| ground-0     ||||
| ground-down-0 ||||

### Naming the tilesets and credits

- All the tileset are prefixed with the source of the tiles.

- For example, each tileset that came from WorkAdventure are named **wa-tileset1.png** or **wa-furnitures.png**. Or opa-tileset.png for openGameArt , opa-LPC-tileset.png from LPC collection…
- Specific tilesets made for an event or a project are named projectname-tileset.png or event-tileset.png

For each tileset, a text file is created (wa-furniture.txt), contening the legal informations (credits, author, license…).


### Putting the map into production (Members access only)

- 

```mermaid
graph TD
 classDef className fill:#f9f,stroke:#333,stroke-width:4px;
    A[MAPS are ready from the map designer]-->B
    B[MAPS are tested online and validated by the client]-->|MAPS are shown to WA| C
    C{Are links / assets / json <br/> already ready for WA ?}
    C -->|YES| D[MAPS are uploaded by the client to WA] -->F
    C -->|NO| E[Feedback and return to the designer] -->|Changes are made by the map designer| A
    F[Maps are checked for production] -->G
    G{Are links / assets / json <br/> already ready for production ?}
    G -->|YES| I[MAPS are optimized by script] -->J
    G -->|NO| E
    J[MAPS are uploaded to S3 in production] -->|Information is given to the client| K
    K[The client validate the version in production] --> M
    M[The client is able to use the production maps]
```
Color code for mermaid chart :

green #43aa8b
dark green foncé #4d908e
blue #277da1
yellow #f9c74f
orange #f3722c
red #f94144

### Some scripting examples

An example of popUp at the entrance of a map.
- The popUp is shown only one time
- The control are blocked until the popUp is validated by the player

var zone0WelcomeName = "popUp0Welcome";
var ispopUp0WelcomeSeen = "no";

// Test if the player already seen this popUp or not
WA.onEnterZone(zone0WelcomeName, () => {
    if(ispopUp0WelcomeSeen == "no"){
        WA.disablePlayerControls();
        ispopUp0WelcomeSeen = "yes";
        currentPopup =  WA.openPopup("popUp0Welcome","Welcome to this WorkAdventure map.",[
            {
                label: "OK",
                className: "popUpElement",
                callback: (popup => {
                // WA.openCoWebSite(urlTeleport);
                // isCoWebSiteOpened = true;
                WA.restorePlayerControls();
                closePopUp();
                })
            }]);
    }
 
})
WA.onLeaveZone(zone0WelcomeName, closePopUp)

### Clients made changes on the map

- They have a local copy of the version in production
- They will follow process and methods to keep the maps "WA" validated.

What is the process to update ?
Can they directly edit maps in production ?

### Cleaning maps after several days / weeks in production

If needeed :

- Join together all individual tiles added as you go along in one tileset.
- Clean all the tiles that are not used anymore

