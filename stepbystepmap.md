## Étapes de création d'une carte avec Github

 * TOUJOURS repartir du repo initial de chez workadventure
  * Il faut créer un repo par projet

## Création du repo sur Github

  * Documentation initiale : https://workadventu.re/map-building

Ouvrir ce lien pour arriver au repo : https://github.com/thecodingmachine/workadventure-map-starter-kit
Puis cliquer sur  use this template

![Screenshot Use Template](img/WATUTO-github-step-by-step-1.png "Use Template")

 * Choisir un nom pour votre projet
 * Cocher : Include all branches
 * Public ou privé ?
 * Cliquer create repository from template
Le repo se génère

## Travailler en local avec ce projet

 * Il est possible d'utiliser plusieurs méthodes (HTTPS, ou via clés SSH pour l'authentification)

### Choix avec HTTPS :

 * Cliquer sur le bouton code / Choisir HTTPS dans le sous onglet

![Screenshot CODE HTTPS SSH](img/WATUTO-github-step-by-step-2.png "CODE HTTPS SSH")

Récupérer le lien https : https://github.com/Jacques-Olivier-Farcy/wa-template.git

Ouvrir une ligne de commande

Installer Tiled, Git et npm (si on veut faire tourner la map en locale)

Copie du repo en local :
Taper git clone https://github.com/Jacques-Olivier-Farcy/wa-template.git

La carte par défaut est map.json

Il est possible de prendre un dossier de map existante et de copier l'intégralité

Le repo est à cette adresse :https://github.com/Jacques-Olivier-Farcy/wa-template


Puis ajouter mes fichiers au repo local :
git add .
git status pour voir l'état du repo local

On commit :
git commit -m "Ajout initial des fichiers du projet"
(-m pour message)

Spécifier l'e-mail :
git config --global user.email "jacques-olivier@ouvaton.org"

Pousser la branche maître :

git push origin master

Sur l'interface web, il y a dans le menu à droite github-pages
Cliquer puis view deployment
La page web qui s'affiche permet de tester la map

Autre moyen d'accéder à la page des liens vers les maps :
Dans github web menu settings puis en bas à gauche pages
Lien du site publié

## Pour lancer la map en local :
Installer npd (node.js download)

npm install
Attendre qq minutes

npm start : permet de travailler directement sur la map

## Avec le client :
Montrer le lien github et  / ou partager l'écran en local.




