## ressources

 * Outils et autres
 * ...

## Outils

### WorkAdventure
  * [Le moteur de rendu des cartes WorkAdventure](https://workadventu.re/create-map.html)
  * [Notes de version](https://github.com/thecodingmachine/workadventure/releases)
  * [La version de dev du moteur](https://staging.workadventu.re/)
  * [Discord](https://discord.gg/YGtngdh9gt)

### Pixel art


  * [Outil de création de la carte Tiled Map Editor](https://www.mapeditor.org/)
  * Pour pixéliser une image : [[http://pixelatorapp.com/|Pixelator]]
  * À utiliser avec le détourage : https://www.remove.bg
  * Mise en ligne des œuvres en 2D : [[https://p3d.in/O4Xyo|p3d in]]
  * Générateur de lettres /mots en pixel art : [[https://www.pixilart.com/draw/| pixiliart]]
  * Générateur de tileset de lettres : [[https://text2image.com/en/|Letters to picture]]
  * Font Pixel Art [[https://fontmeme.com/pixel-fonts/|Font Pixel Art]]
  * Combiner des tuiles : [[https://www.peko-step.com/en/tool/combine-images.html| Merge tile]]
    * https://codeshack.io/images-sprite-sheet-generator/
  * à voir pour Raspberry :
    * https://github.com/patriciogonzalezvivo/glslViewer
    * https://github.com/patriciogonzalezvivo/glslViewer/wiki/Compiling-with-video-support
  * Bibliothèque de sons libres : https://freesound.org
  * Convertisseur audio : https://js-audio-converter.com/fr

  * Générateur de planètes : https://deep-fold.itch.io/pixel-planet-generator
  * Planètes en pixel art : https://helianthus-games.itch.io/pixel-art-planets
  * Planète type terre : https://gold356.itch.io/earh-rotating-32-x-32

Pour les stream :

  * [[https://korben.info/obs-ninja-streamer-camera.html|OBS Ninja]] et [[https://github.com/steveseguin/obsninja/wiki/FAQ#whatis|OBS Ninja FAQ]]
  * [[https://obsproject.com/|OBS project]]
  * Simuler une webcam locale à partir d'un stream http pour Windows : [[https://ip-webcam.appspot.com/|IP camera Adapter]]
  * [[https://dev47apps.com/obs/|Use Android phone as stream cam]]
  * [[https://blog.jbrains.ca/permalink/using-obs-studio-as-a-virtual-cam-on-linux|obs studio cam linux]]
  * droidcam : https://www.dev47apps.com/

  ## Assets

  Quelques exemples :
  * [[https://play.workadventu.re/_/global/rc3-data.hackerspace.pl/main.json | hackerspace]]
  * [[https://play.meet.n2n.io/_/global/raw.githubusercontent.com/Binary-Kitchen/rc3.world/master/main.json | rc3]]
  * [[https://play.workadventu.re/_/global/gparant.github.io/tcm-client/Demo/demo-v1.json | Demo workadventure]]
  * [[https://github.com/workadventure-xce/workadventure-xce/|Fork communautaire]]

    * Des arbres, beaucoup d'arbres : [[https://opengameart.org/content/trees-mega-pack-cc-by-30-0|OpenGameArt trees]]
  * [[https://opengameart.org/content/consolidated-hard-vacuum-terrain-tilesets|OpenGameArt terrain]]
  * [[https://opengameart.org/content/lpc-terrain-repack| OpenGameArt LPC terrain]]
  * [[https://malibudarby.itch.io/|Malibudarby graphics]]

Ressources en ligne :
  * https://cartographyass
  * https://opengameart.org/
  * http://charas-project.net
  * http://refmap-l.blog.jp/
  * http://www.untamed.wild-refuge.net/rmxpresources.php?characters
  * https://pixelation.org/
  * https://iconduck.com
  * https://www.spriters-resource.com
  * http://pixelartmaker.com/gallery
  * https://opengameart.org/content/sci-fi-object-set
  * https://assetstore.unity.com/?q=top-down%20tileset&orderBy=1
  * Sprite : https://www.deviantart.com/geminidrake/art/RPG-Maker-VX-Sprite-Nagisa-Hazuki-551408414

  * Les assets de WorkAdventure : https://github.com/thecodingmachine/workadventure/tree/develop/front/dist/resources
